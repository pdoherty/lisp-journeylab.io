+++
date = "2022-04-21T07:51:49+01:00"
title = "Resources"
draft = false
+++


* [lisp-lang.org](http://lisp-lang.org/)
* [Common Lisp Cookbook](https://lispcookbook.github.io/cl-cookbook/)
* [awesome-cl](https://github.com/CodyReichert/awesome-cl)
  * [learning and tutorials](https://github.com/CodyReichert/awesome-cl#learning-and-tutorials)
      * [Lisp project of the day](http://40ants.com/lisp-project-of-the-day/)
      * [Google's Lisp koans](https://github.com/google/lisp-koans)
      * [Lisp tips](https://github.com/lisp-tips/lisp-tips/issues/) a GitHub repository where anybody can post tips an tricks.

search libraries on

* http://quickdocs.org/

## Individual sites:

* [sjl's road to Lisp](http://stevelosh.com/blog/2018/08/a-road-to-common-lisp/)
* [Martin Cracauer's Gentle Introduction to compile-time computing](https://medium.com/@MartinCracauer/a-gentle-introduction-to-compile-time-computing-part-3-scientific-units-8e41d8a727ca) - excellent article series

## Screencasts:

Of course, see **[my Udemy Lisp course](https://www.udemy.com/course/common-lisp-programming/?referralCode=2F3D698BBC4326F94358)**!

I also post videos on Youtube, check this out:

- [How to quickly create a Common Lisp project with my project generator](https://www.youtube.com/watch?v=XFc513MJjos): see an ASDF system definition, how to build a binary, run the project from sources, load everything in Emacs, run the test suite…
- [How to interactively fix failing tests](https://www.youtube.com/watch?v=KsHxgP3SRTs) - very short video to showcase the interactive debugger and that we can re-compile a failing function and resume the execution from where it failed to see it finally pass.

<iframe width="560" height="315" src="https://www.youtube.com/embed/XFc513MJjos" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Those ones ar great too:


* [Little Bits of Lisp](https://www.youtube.com/playlist?list=PL2VAYZE_4wRJi_vgpjsH75kMhN4KsuzR_) -
  short videos on various topics: inspecting a condition, basics of
  lisp's evaluation model,…
* [Common Lisp Tutorials](https://www.youtube.com/playlist?list=PL2VAYZE_4wRIoHsU5cEBIxCYcbHzy4Ypj), of which [Emacs and Slime - useful keyboard shortcuts](https://www.youtube.com/watch?v=sBcPNr1CKKw&index=4&list=PL2VAYZE_4wRIoHsU5cEBIxCYcbHzy4Ypj)
* [Programming a message bus in Common Lisp](https://www.youtube.com/watch?v=CNFr7zIfyeM) - shows the interactive nature of lisp, good use of the debugger, test-driven development to shape the api, bordeaux-threads.
* [Marco Baringer's SLIME Tutorial Video](https://www.youtube.com/watch?v=_B_4vhsmRRI) - a video showing Marco develop a package and explaining Slime and Lisp features, with many little important explanations (1 hour). It only has some rotten bits, for example it installs packages with asdf-install and not Quicklisp.
* [Web development in Emacs with Common Lisp and ClojureSCript](https://www.youtube.com/watch?v=bl8jQ2wRh6k) -
  building [Potato](https://github.com/cicakhq/potato), a Slack-like app.
* Shinmera playlists:
  [Treehouse](https://www.youtube.com/playlist?list=PLkDl6Irujx9MtJPRRP5KBH40SGCenztPW)
  (game dev),
  [Demos](https://www.youtube.com/playlist?list=PLkDl6Irujx9Mh3BWdBmt4JtIrwYgihTWp)
  (of small programs).
* [Pushing pixels with Lisp](https://www.youtube.com/watch?v=82o5NeyZtvw), and by the same author:
 * [CEPL demo](https://www.youtube.com/watch?v=a2tTpjGOhjw&index=20&list=RDxzTH_ZqaFKI) - working with OpenGL
 * [Baggers' channels](https://www.youtube.com/channel/UCMV8p6Lb-bd6UZtTc_QD4zA).
* [Cando: computational chemistry with Common Lisp on LLVM with Jupyter notebooks](https://www.youtube.com/playlist?list=PLbl4KVdl9U3I3MhFWgauT0cz-x7SymZmn&disable_polymer=true)

* [McClim interactive GUI demos](https://www.youtube.com/watch?v=XGmo0E_S46I). [Code examples](https://github.com/robert-strandh/McCLIM/blob/master/Examples/demodemo.lisp). Presentation of [Clim listener, Clim debugger, drawing objects into the GUI repl](https://www.youtube.com/watch?v=kfBmRsPRdGg).
* [videos of the European Lisp Symposium](https://www.youtube.com/channel/UC55S8D_44ge2cV10aQmxNVQ)

and more on [Cliki](http://www.cliki.net/Lisp%20Videos).

## Some games:

* [Kandria](https://kandria.com/) - a nice platform game being actively developed and launching soon. Check it out!

* [Spycursion](https://defungames.com/) - "a sandbox "edutainment" MMO centered around hacking and espionage which takes place in a near-future world".


<img src="https://defungames.com/wp-content/uploads/2019/01/eavesdropping.png" alt="spycursion" width="450"/>

* [dmomd](https://codeberg.org/artchad/dmomd) - A rogue-like RPG with turn based movement and combat.

![](https://codeberg.org/artchad/dmomd/media/branch/master/screenshots/screen-001-480x270.jpg)

* [Jettatura](https://store.steampowered.com/app/2023440/Jettatura/) - a challenging first-person dungeon-crawler (DRPG) with round-based combat. On Steam and planned for 2022, Q2.

* [Orb](https://baggers.itch.io/orb)

![](https://img.itch.zone/aW1hZ2UvMjUyMTE3LzEyMDYxNDUucG5n/347x500/YEFnGW.png)

* [Notalone](https://github.com/borodust/notalone)

![](https://img.itch.zone/aW1hZ2UvMTg2MzQ1Lzg3MjI2NC5wbmc=/347x500/6WLhIo.png)

* [Moppu](https://sarge.itch.io/moppu)

![](https://img.itch.zone/aW1hZ2UvNTAyMzQ2LzI2MDA1NjUucG5n/347x500/hdXQvV.png)

* [The price of a cup of coffee](https://goofist.itch.io/the-price-of-a-cup-of-coffee)

![](https://img.itch.zone/aW1hZ2UvNTAyNzk3LzI2MDMzNDQucG5n/347x500/0svE3f.png)

* http://www.sebity.com/projects.php (Snake, the Invaders,… with OpenGL)

* [cl-snake](https://github.com/SahilKang/cl-snake) snake in the terminal

* [Fruktorama](https://github.com/lisp-mirror/fruktorama), Tetris-like with fruits.

![](https://camo.githubusercontent.com/f14f4bd34f1b41942c4dd9fdece3f7ea448e2f94f40fb040c7b03b230d6f3452/68747470733a2f2f6269746275636b65742e6f72672f50617261736974654e6574776f726b2f6672756b746f72616d61332d6c6973702d65646974696f6e2f7261772f306465613761623834326665306638616530336262636564356266316237636633353832343961392f73637265656e73686f74732f73637265656e73686f742d322e706e67)
