---
title: "You can buy a preview of the Common Lisp Cookbook in ePub"
date: 2020-11-13T22:48:15+01:00
draft: false
---

![](https://storage.ko-fi.com/cdn/useruploads/display/39a60d12-fa99-409d-873f-853318f811d8_orly-cover.png)

**See here: https://ko-fi.com/s/01fee22a32**

Let me try something: I propose you here to buy the ePub, even though it is meant to be available for free.

I contributed quite a lot to the Cookbook and I found since the
beginning that having an EPUB and/or a PDF version would be very
useful. Some years later, nobody did it, and I finally wrote a script
to bundle all the pages together and generate an ePub, and then a
PDF. It isn't finished though, it needs more editing, and it would
also be great to write a proper LaTeX style for the PDF. As I have one
version on disk, I thought on giving potential supporters the
opportunity to read it and fund the remaining work, or simply give a sign of
encouragement. It's also the opportunity for me to try this new Ko-fi
feature, and to practice speaking about financial support…

Let's turn it another way: if you support me, here's a reward for you, an exclusivity.
And in any case, hold on, this ePub will eventually be available for everybody.

Thanks!

---

ps: About money: yes, it does a difference right now, because I don't
have a fixed, nor big, income. I am trying to live on my free software
activities. I have a few clients, but not enough, so I'm trying to
diversify and get some earnings from my Lisp work. I even pay lisp
contributors to write free software (we are currently adding Stripe
payments to a web application. When it's done, you'll know everything
about it). So, the money has an impact.
