---
title: "Video: Create a Common Lisp project from scratch with our project generator 🎥"
date: 2022-05-11T11:43:31+02:00
draft: false
tags: ["tutorial"]
---

In this video I want to demo real-world Lisp stuff I had trouble finding tutorials for:

- how to create a CL project:
  - what's in the .asd file?
  - what's a simple package definition?
  - how do we load everything in our editor (Emacs and SLIME here)?
- how to set up tests?
  - and how to run them from the terminal?
  - and (WTF) how to get the correct exit code????
- how to build a binary in order to run our app from the terminal?
  - and so, how to get command-line arguments?
  - but also, can I run my app from sources, without building a binary?
- what's Roswell and how do I share my app with it?

Let's find out (it's 5 minutes long):

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/XFc513MJjos" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

I use my project generator:
[cl-cookieproject](https://github.com/vindarel/cl-cookieproject). See
setup, alternatives, limitations and TODOs there. You will also find a
similar generator for web projects.

If you find it useful, share the video!
