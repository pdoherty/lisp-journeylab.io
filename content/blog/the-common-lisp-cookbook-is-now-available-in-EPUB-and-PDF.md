---
title: "The Common Lisp Cookbook Is Now Available in EPUB and PDF"
date: 2021-01-19T11:32:25+01:00
draft: false
---

I am glad to announce that the Common Lisp Cookbook is now available in ePub and PDF.

It is available for free, and you can pay what you want[1] to say a
loud "thank you" and to further support its development. Thanks!

This EPUB represents the work on the span of three years where I have been constantly reading, experimenting, asking, discovering tips, tools, libraries and best-practices, built-in or not, all of which should have been easily accessible but were not. Now they are. Reviving the Cookbook project resonated in the community, as other lispers sent great contributions.


<a style="font-size: 16px; background-color: #4CAF50; color: white; padding: 16px; cursor: pointer;" href="https://ko-fi.com/s/01fee22a32">
  Donate and download the EPUB version
</a>


=> https://lispcookbook.github.io/cl-cookbook/ <=

---

[1]: above 6 USD actually.
