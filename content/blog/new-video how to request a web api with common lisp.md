---
title: "New video: how to request a REST API in Common Lisp: fetching the GitHub API 🎥"
date: 2022-06-11T11:43:31+02:00
draft: false
tags: ["tutorial", "web", "libraries"]
---

A few weeks ago, I put together a new Lisp video. It's cool, sound on, 'til the end ;)

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/TAtwcBh1QLg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

I want to show how to (quickly) do practical, real-world stuff in
Lisp. Here, how to request a web API. We create a new full-featured
project with my project skeleton, we study the GitHub API, and we go
ahead.

I develop in Emacs with Slime, but in the end we also build a
binary, so we have a little application that works on the command line
(note that I didn't use SBCL's core compression, we could have a
lighter binary of around 30MB).

I use the libraries: Dexador for HTTP requests, the Jonathan and then
the Shasht JSON libraries, as well as Access and Serapeum to help with
accessing, creating and viewing hash-tables.

Thanks for the comments already :)

>  tks for that... it's amazing.

> excellent

>  Thank you, that was a great video! I plan to get the udemy course now based on this.


Here are my previous ones:

- [How to create a new full-featured Common Lisp project (with my project generator)](https://www.youtube.com/watch?v=XFc513MJjos)
- [Interactively fixing failing tests](https://www.youtube.com/watch?v=KsHxgP3SRTs)

I want to do more but damn, it takes time. You can push me for more :)


Last notes:

I had fun with the soundtrack :)

I edit the video with the latest [Kdenlive](https://kdenlive.org/) with the snap package and it's great for me.
